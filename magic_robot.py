#!/usr/bin/env python3
# exec(open("magic_robot.py").read())
# to reload a module in shell:
# import sys
# from importlib import reload
# reload(sys.modules['missions'])

from ev3dev2.motor import MoveTank, LargeMotor, MediumMotor
from ev3dev2.motor import list_motors
from ev3dev2.motor import OUTPUT_A, OUTPUT_B, OUTPUT_C, OUTPUT_D
from ev3dev2.sensor.lego import GyroSensor, ColorSensor
from ev3dev2.sensor import INPUT_1, INPUT_2, INPUT_3, INPUT_4
from ev3dev2.button import Button
from time import sleep, time
from math import pi, floor
from PIDReg import PIDReg
# from missions import do_mission, mission_one, mission_two, mission_three, color_calibration
# from ev3dev2.sound import Sound
# from missions import mission_one, mission_two

# stop all the motors with the 'stop' command
# the mottors will have the 'brake' on
# made to be called before unexpected program brake
def exit_handler():
    for motor in list_motors():
        motor.command = 'stop'
    print(" END ")

# raise KyboardInterrupt when left or right key is pressed
# need to be pulled using Button.process(state) function
def left(state):
    if state:
        raise KeyboardInterrupt("STOP")
def right(state):
    if state:
        raise KeyboardInterrupt("STOP")


# main class, defines the attributes and methods of the Robot
class Robot:
    # init function, called once when a new object is made
    def __init__(self, polarity = 'inversed', diameter = 5.6):
        self.black = 4
        self.white = 61.5
        self.zero = 0
        self.color_sensitivity = 10
        self.distance_sensitivity = 0
        self.polarity = polarity
        self.diameter = diameter
        self.walk_PID_angle = PIDReg(KP = 2.8)
        self.walk_PID_velocity = PIDReg(KP = 0.4)
        self.walk_PID_distance = PIDReg(KP = 0.2)
        self.set_motors()
        self.set_sensors()
        self.buttons = Button()
        self.buttons.on_left = left
        self.buttons.on_right = right
        self.SPEED_BOOST = 1.2

    # link the motors to the output ports, has default parameters
    def set_motors(self, left_motor = OUTPUT_C, right_motor = OUTPUT_B, medium_left = OUTPUT_A, medium_right = OUTPUT_D):
        self.motors = MoveTank(left_motor, right_motor)
        self.left_motor = LargeMotor(left_motor)
        self.right_motor = LargeMotor(right_motor)
        self.medium_left = MediumMotor(medium_left)
        self.medium_right = MediumMotor(medium_right)
        self.left_motor.polarity = self.polarity
        self.right_motor.polarity = self.polarity
    # link the sensors to the input ports, has default parameters
    # also resets the gyro sensors
    def set_sensors(self, gyro = INPUT_2, left_sensor = INPUT_3, right_sensor = INPUT_4):
        self.left_sensor = ColorSensor(left_sensor)
        self.right_sensor = ColorSensor(right_sensor)
        self.gyro = GyroSensor(gyro)
        self.gyro.mode = 'GYRO-CAL'
        sleep(1)
        self.gyro.mode = 'GYRO-ANG'
    # driving function for motors
    # work like moveTank
    # make sure that the speed does not exeed the max_speed in any direction
    def my_motors(self, left_speed, right_speed, max_speed = 90):
        self.buttons.process()
        if left_speed > max_speed:
            left_speed = max_speed
        if left_speed < -max_speed:
            left_speed = -max_speed
        if right_speed > max_speed:
            right_speed = max_speed
        if right_speed < -max_speed:
            right_speed = -max_speed
        self.motors.on(left_speed, right_speed)

    # stops all the motors with default brake=False
    def off_motors(self, brake=False):
        self.left_motor.off(brake=brake)
        self.right_motor.off(brake=brake)
        self.medium_left.off(brake=brake)
        self.medium_right.off(brake=brake)

    # read and return the speed
    # for Large Motors the returned value is close to percentage
    # for Medium Motors is aprox x1.5 percentage value
    def read_speeds(self):
        return self.left_motor.speed/10, self.right_motor.speed/10
    
    # function for reading the gyro sensors
    # is made such that the sensor will not be reseteted when we read speed or position
    def my_gyro(self, mode='angle', cero = None):
        angle, rate = self.gyro.rate_and_angle # ! should be angle_and_rate but
        if cero == None:
            cero = self.zero
        if mode == 'angle':
            return angle - cero
        elif mode == 'rate':
            return rate
        elif mode == 'angle_and_rate':
            return angle - cero, rate
        else:
            print("ERROR: UNKNOWN MODE 'angle' 'rate' OR 'angle_and_rate'")

    # function for calibrate the light sensors
    def light_calibrate(self):
        print('Calibration, ready!')
        print('White')
        input()
        input()
        self.left_sensor.calibrate_white()
        self.right_sensor.calibrate_white()
        sleep(0.5)
        max1 = self.left_sensor.reflected_light_intensity 
        max2 = self.right_sensor.reflected_light_intensity
        print('Black, ready!')
        input()
        # input()
        min1 = self.left_sensor.reflected_light_intensity
        min2 = self.right_sensor.reflected_light_intensity
        self.white = (max1+max2)/2
        self.black = (min1+min2)/2
        print('White ', self.white)
        print('Black ', self.black)
        input()
        input()

    # function for reading the light sensor
    # can use red light, white light or can return RGB color
    def light(self, sensor, mode='red'):
        if sensor == 'left':
            sensor = self.left_sensor
        elif sensor == 'right':
            sensor = self.right_sensor
        else:
            if isinstance(sensor, ColorSensor):
                pass
            else:
                print("COLOR SENSOR ERROR: 'left' 'right' OR ColorSensor OBJECT ")

        if mode == 'red':
            return (sensor.reflected_light_intensity - self.black) * (100/(self.white-self.black))
        elif mode == 'white':
            R, G, B = sensor.rgb
            return (R+G+B)/7.65
        elif mode == 'rgb':
            return sensor.rgb
        else:
            print(" COLOR MODE ERROR 'red' OR 'rgb' OR 'white' ")
            return None
    
    # speed correction function
    def walk_open_loop(self, set_point, base_speed):
        # self.print_color('left')
        if self.left_motor.is_overloaded or self.right_motor.is_overloaded:
            left_speed, right_speed = self.read_speeds()
            if base_speed > 0:
                base_speed = min(left_speed, right_speed) - 5
            else:
                base_speed = max(left_speed, right_speed) - 5
        angle, velocity = self.my_gyro('angle_and_rate') 
        angle_error = set_point - angle
        velocity_error = 0 - velocity
        correction = self.walk_PID_angle.regulate(angle_error) + self.walk_PID_velocity.regulate(velocity_error)
        left_speed = base_speed - correction
        right_speed = base_speed + correction
        self.my_motors(left_speed, right_speed)

    # power correction function
    def walk_open_loop2(self, set_point, base_speed):
        angle, velocity = self.my_gyro('angle_and_rate') 
        angle_error = set_point - angle
        velocity_error = 0 - velocity
        correction = self.walk_PID_angle.regulate(angle_error) + self.walk_PID_velocity.regulate(velocity_error)
        left_speed = base_speed - correction
        right_speed = base_speed + correction
        self.power_motors(left_speed, right_speed)

    # function for accelerating or deccelerating
    def change_speeds(self, new_left_speed, new_right_speed, acceleration_time = 20):
        # acceleration time in microseconds
        left_speed, right_speed = self.read_speeds()
        left_change = new_left_speed - left_speed
        right_change = new_right_speed - right_speed
        numitor = max(abs(left_change), abs(right_change))
        left_rate = left_change / numitor
        right_rate = right_change / numitor
        acceleration_time *= 1000000
        while numitor >= 0:
            numitor -= 1
            left_speed += left_rate
            right_speed += right_rate
            self.my_motors(left_speed, right_speed)
            sleep(acceleration_time)
        else:
            self.my_motors(new_left_speed, new_right_speed)
        left_speed, right_speed = self.read_speeds()

    # my sleep function, also pulls the left and right buttons
    def my_sleep(self, target_time):
        init_time = time()
        while time() - init_time < target_time:
            self.buttons.process()

    # stop timer function, return True when target time is reached
    def timed_stop(self, target_time, init = False):
        if(init):
            self.initial_time = time()
        else:
            if(time() - self.initial_time > target_time):
                return True
        return False

    # stop function for distance straight walk
    # control the last bit of movement and stops when target reached
    def distance_stop(self, target_distance, set_point, init = False):
        if init:
            self.sign = lambda x: (1, -1)[x < 0]
            self.direction = self.sign(target_distance)
            self.degrees = target_distance*360/(self.diameter*pi) + (self.left_motor.degrees + self.left_motor.degrees)/2
            self.go_easy = self.degrees - self.direction*5*360/(self.diameter*pi)   
        else:
            if self.direction*(self.left_motor.degrees + self.left_motor.degrees)/2 > self.direction*self.go_easy:
                speed_left, speed_right = self.read_speeds()
                base_speed = (speed_left + speed_right) / 2
                speed = base_speed
                I = 0
                while((speed > self.distance_sensitivity or speed < -self.distance_sensitivity)):
                    error = self.degrees - (self.left_motor.degrees + self.left_motor.degrees)/2
                    correction = self.walk_PID_distance.regulate(error) + I*0.05
                    speed = self.sign(correction)*floor(min(abs(correction), abs(base_speed)))
                    self.walk_open_loop(set_point, base_speed = speed)
                    I = I*0.7 + error
                return True
        return False

    # stop function for walking until a treeshold value is reached
    # when the sensor value is close to the treeshold value, return True
    def color_stop(self, treeshold, sensor, mode='red', init = False):
        sensor_value = self.light(sensor, mode=mode)
        # print('Sensor: ', sensor_value)
        if init:
            pass
        else:
            if sensor_value < treeshold + self.color_sensitivity and sensor_value > treeshold - self.color_sensitivity:
                return True
        return False

    # universal straight_walk function
    # needs a stop function to stop the movement
    def straight_walk(self, set_point, base_speed, stop_function, *args, brake=True, mode='power'):
        stop_function(*args, init = True)
        base_speed *= self.SPEED_BOOST
        if mode == 'power':
            loop_function = self.walk_open_loop2
        elif mode == 'speed':
            loop_function = self.walk_open_loop
        else:
            print("UNKOWN MODE, MUST BE: 'power' OR 'speed'")
        while(stop_function(*args) == False):
            loop_function(set_point = set_point, base_speed = base_speed)

        if brake:
            self.motors.off()
        # self.motors.off()
        # input()
        
    # redefinition of straight_walk function
    # for distance, timed and color stop functions
    # made such these will be easier to use
    def distance_straight_walk(self, angle, speed, distance, brake=True, mode='power'):
        self.straight_walk(angle, speed, self.distance_stop, distance, angle, brake=brake, mode=mode)

    def timed_straight_walk(self, angle, speed, delay, brake=True, mode='power'):
        self.straight_walk(angle, speed, self.timed_stop, delay, brake=brake, mode=mode)

    def color_straight_walk(self, angle, speed, treeshold, sensor, brake=True, mode='power'):
        if sensor == 'left':
            sensor = self.left_sensor
        elif sensor == 'right':
            sensor = self.right_sensor
        else:
            print(" ERROR, sensor should be either 'left' or 'right' ")
            return 0
        self.straight_walk(angle, speed, self.color_stop, treeshold, sensor, brake=brake, mode=mode)

    # command the main motors in power mode
    # be sure that the power does not exeed the max_power in any direction
    def power_motors(self, power_left, power_right, max_power = 100, brake=False):
        self.buttons.process()
        if power_left > max_power:
            power_left = max_power
        if power_left < -max_power:
            power_left = -max_power
        if power_left == 0:
            self.left_motor.off(brake=brake)
        else:
            self.left_motor.duty_cycle_sp = power_left
            self.left_motor.run_direct()
        if power_right > max_power:
            power_right = max_power
        if power_right < -max_power:
            power_right = -max_power
        if power_right == 0:
            self.right_motor.off(brake=brake)
        else:
            self.right_motor.duty_cycle_sp = power_right
            self.right_motor.run_direct()

    # function for spinning the robot to a given angle
    def spin(self, target, max_speed = 50, KP=0.8):
        # print()
        # print(self.my_gyro())
        sensor = self.my_gyro()
        error = target - sensor
        speed_left = 0
        speed_right = 0
        while (abs(error) > 0):
            speed = min(error*KP, max_speed)
            speed_left = -speed
            speed_right = speed
            self.my_motors(speed_left, speed_right)
            sensor = self.my_gyro()
            error = target - sensor
        self.motors.off()
        # input()
    
    # function to start a motor until the motor is stalled
    # this function operates in speed, thus, the motor must be blocked at the maximum power
    def stall_the_motor(self, speed, motor, brake=False):
        if motor == 'medium_left':
            motor = self.medium_left
        elif motor == 'medium_right':
            motor = self.medium_right
        elif motor == 'left':
            motor = self.left_motor
        elif motor == 'right':
            motor = self.right_motor
        else:
            print("UNKNOWN MOTOR; we have: 'medium_left' 'medium_right' 'left' 'right'")
            return 0
        motor.on(speed)
        sleep(0.1)
        while not motor.is_stalled:
            sleep(0.01)
        motor.off(brake)

    # function to power a motor until the motor is stalled
    # this function operates in powerd, thus, the motor must be blocked at the given power
    def stall_the_motor2(self, power, motor, brake=False):
        if motor == 'medium_left':
            motor = self.medium_left
        elif motor == 'medium_right':
            motor = self.medium_right
        elif motor == 'left':
            motor = self.left_motor
        elif motor == 'right':
            motor = self.right_motor
        else:
            print("UNKNOWN MOTOR; we have: 'medium_left' 'medium_right' 'left' 'right'")
            return 0
        if power > 100:
            power = 100
        elif power < -100:
            power = -100
        motor.duty_cycle_sp = power
        motor.run_direct()
        sleep(0.1)
        while not motor.is_stalled:
            sleep(0.01)
        motor.off(brake=brake)

    # print the color in RGB
    # try to paint the text in the seen color
    def print_color(self, sensor):
        if sensor == 'left':
            sensor = self.left_sensor
        elif sensor == 'right':
            sensor = self.right_sensor
        else:
            print(" ERROR, sensor should be either 'left' or 'right' ")
            return 0
        R, G, B = self.light(sensor, mode='rgb')
        print('\033[38;2;' + str(R) + ';' + str(G) + ';' + str(100) + 'm', R, G, B, '\033[38;2;0m')

try:
    if __name__ == "__main__":
        init_time = time()
        robby = Robot()
        # mission_three(robby)
        print(time()-init_time)
finally:
        exit_handler()