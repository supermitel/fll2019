#!/usr/bin/env python3
# exec(open("main.py").read())

from missions import do_mission, mission_one, mission_two, mission_three, color_calibration
from ev3dev2.button import Button
from magic_robot import Robot, exit_handler
import os
import sys
from time import sleep

mission = 1
MISSION_NUMBER = 4
robby = Robot()
buttons = Button()

def up(state):
# if buttons.up:
    global mission
    os.system('clear')
    if not state:
        if mission > -1:
            mission -= 1
        else:
            mission = MISSION_NUMBER
        if mission == 0:
            print('Color Calibration')
        elif mission == -1:
            print('Reset Gyro')
        else:
            print('M ', mission)
    return 0

def down(state):
# if buttons.down:
    global mission
    os.system('clear')
    if not state:
        if mission < MISSION_NUMBER:
            mission += 1
        else:
            mission = 1
        if mission == 0:
            print('Color Calibration')
        elif mission == -1:
            print('Reset Gyro')
        else:
            print('M ', mission)
    return 0
    
def enter(state):
# if buttons.enter:
    # global mission
    os.system('clear')
    print('Do M ', mission)
    if not state:
        do_mission(robby, mission)
        down(False)
    os.system('clear')
    print("M ", mission)
    return 0

def main():
    buttons.on_up = up
    buttons.on_down = down
    buttons.on_enter = enter
    os.system('setfont Lat15-TerminusBold32x16')
    print("Ready!")
    print('M ', mission)
    while True:
        buttons.process()
        sleep(0.01)

if  __name__ == "__main__":
    main()