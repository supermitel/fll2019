#!/usr/bin/env python3
# exec(open("missions.py").read())

from magic_robot import Robot, exit_handler
from PIDReg import PIDReg
import os

def color_calibration(robot):
    robot.light_calibrate()

def mission_one(robot):
    robot.power_motors(70, 100)
    robot.stall_the_motor2(30, 'medium_right', brake=True)
    robot.my_sleep(4)
    robot.motors.off(brake=True)
    robot.my_sleep(1.5)
    robot.power_motors(100, 100)
    robot.my_sleep(0.5)
    robot.medium_right.on_for_degrees(-20, 400)
    robot.power_motors(20, 20)
    robot.my_sleep(2)
    robot.power_motors(-30, -30)
    robot.my_sleep(1)
    robot.power_motors(-100, -100)
    robot.my_sleep(1.7)
    robot.medium_right.on_for_degrees(20, 400)
    robot.off_motors()

def mission_two(robot):
    angle = 0
    robot.distance_straight_walk(angle, 80, 70)
    robot.color_straight_walk(angle, 80, 95, 'left')
    robot.distance_straight_walk(angle, 50, 6)
    angle += 90
    robot.spin(angle)
    robot.color_straight_walk(angle, 80, 95, 'left')
    # daca nu ajunge bine la cele 4 discuri, modifica mai jos
    # daca modifici, nu uita sa modifici si la intoarcere
    robot.distance_straight_walk(angle, 80, 40.5)
    angle += 90
    robot.spin(angle)
    robot.distance_straight_walk(angle, 70, 12)
    robot.timed_straight_walk(angle, 20, 3, mode='power')
    robot.timed_straight_walk(angle, -10, 0.61, mode='speed')
    robot.medium_right.on_for_degrees(-80, 475)
    robot.medium_left.on_for_degrees(-30, 125)
    robot.medium_right.on_for_degrees(80, 475, block=False)
    robot.distance_straight_walk(angle, -15, -25, mode='speed')
    # adica aici
    angle += 60
    robot.spin(angle)
    robot.distance_straight_walk(angle, 100, 52)
    angle = 180
    robot.spin(angle)
    robot.distance_straight_walk(angle, 100, 58)  
    robot.off_motors()

def mission_three(robot):
    angle = 0
    robot.distance_straight_walk(angle, 100, 60)
    robot.color_straight_walk(angle, 70, 100, 'left')
    robot.color_straight_walk(angle, 15, 10, 'left')
    robot.off_motors(brake=True)
    robot.right_motor.on(15)
    while robot.color_stop(0, 'right') == False:
        pass
    robot.right_motor.off(brake=True)
    robot.left_motor.on(-10)
    while robot.color_stop(0, 'left') == False:
        pass
    robot.left_motor.off(brake=True)
    # input()
    robot.medium_right.on_for_degrees(-30, 240)
    robot.my_sleep(1)
    robot.medium_left.on_for_degrees(-10, 400)
    angle = robot.my_gyro()
    robot.timed_straight_walk(angle, 10, 2, mode='speed')
    robot.off_motors(brake=True)
    robot.medium_left.on_for_degrees(20, 150, brake=False)
    robot.medium_left.on_for_degrees(7, 400)
    robot.my_sleep(0.5)
    robot.medium_left.on(-7)
    robot.my_sleep(4)
    robot.medium_left.off()
    robot.medium_right.on_for_seconds(30, 0.43)
    robot.medium_right.off(brake=False)
    robot.distance_straight_walk(angle, -30, -12)
    angle=0
    robot.spin(angle)
    robot.timed_straight_walk(angle, -100, 3)
    robot.medium_left.on_for_degrees(30, 800)
    robot.off_motors()

def mission_four(robot):
    angle = 0
    robot.distance_straight_walk(angle, 100, 80)
    robot.color_straight_walk(angle, 70, 95, 'left')
    angle += 90
    robot.spin(angle)
    robot.color_straight_walk(angle, 70, 95, 'left')
    robot.distance_straight_walk(angle, 70, 42)
    angle = 0
    robot.spin(angle)
    robot.color_straight_walk(angle, 20, 95, 'left')
    angle = -40
    robot.spin(angle)
    robot.color_straight_walk(angle, 60, 108, 'left')
    robot.distance_straight_walk(angle, 30, 12, mode='speed')
    angle = 0
    robot.spin(angle)
    robot.medium_right.on_for_degrees(10, 400)
    robot.medium_right.on_for_degrees(-10, 500)
    robot.my_sleep(2)
    robot.spin(angle)
    robot.color_straight_walk(angle, 22, 0, 'right', mode='speed')
    robot.distance_straight_walk(angle, 30, 18, mode='speed')
    angle += 30
    robot.spin(angle)
    robot.medium_left.on_for_degrees(-100, 100)
    robot.my_sleep(1)
    robot.medium_left.on_for_degrees(100, 100)
    # input()
    robot.color_straight_walk(angle, -30, 100, 'left', mode='speed')
    angle = -53
    robot.spin(angle)
    robot.distance_straight_walk(angle, 50, 30)
    robot.distance_straight_walk(angle, 15, 20, mode='speed')
    robot.off_motors()

def do_mission(robot, mission):
    robot.zero = robot.my_gyro(cero=0)
    robot.my_sleep(0.4)
    print(robot.my_gyro())
    if mission == -1:
        try:
            robot.set_sensors()
        except KeyboardInterrupt:
            robot.off_motors()
    elif mission == 0:
        try:
            color_calibration(robot)
        except KeyboardInterrupt:
            robot.off_motors()
    elif mission == 1:
        try:
            mission_one(robot)
        except KeyboardInterrupt:
            robot.off_motors()
    elif mission == 2:
        try:
            mission_two(robot)
        except KeyboardInterrupt:
            robot.off_motors()
    elif mission == 3:
        try:
            mission_three(robot)
        except KeyboardInterrupt:
            robot.off_motors()
    elif mission == 4:
        try:
            mission_four(robot)
        except KeyboardInterrupt:
            robot.off_motors()
    else:
        os.system('clear')
        print("UNKNOWN MISSION!")
    return 0

try:
    if __name__ == "__main__":
        # init_time = time()
        robby = Robot()
        do_mission(robby, 4)
        # mission_four(robby)
        # print(time()-init_time)
finally:
        exit_handler()